<?php
Class Jaime extends Lannister
{
	public function with( $obj )
	{
		if (get_parent_class($obj) == 'Stark')
		{
			return ("Let's do this");
		}
		if (get_class($obj) == "Cersei")
		{
			return ("With pleasure, but only in a tower in Winterfell, then");
        }
        return ("Not even if I'm drunk !");
	}
}
?>